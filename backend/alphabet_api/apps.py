from django.apps import AppConfig


class AlphabetApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "alphabet_api"
