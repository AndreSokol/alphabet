from django.urls import path

from . import views

app_name = "api"

urlpatterns = [
    path("", views.index, name="index"),
    path("exercise/<uuid:exercise_id>/", views.exercise, name="exercise"),
]
