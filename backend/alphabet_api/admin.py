from django.contrib import admin

from .models import ExerciseText

admin.site.register(ExerciseText)
