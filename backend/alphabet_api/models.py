from django.db import models

from core.models import UUIDKeyedModel


class ExerciseText(UUIDKeyedModel):
    title = models.TextField()
    text = models.TextField()
    source_description = models.CharField(null=True, blank=True, max_length=255)
    source_url = models.URLField(null=True, blank=True)

    preview = models.ImageField(upload_to='exercise_preview', null=True, blank=True)

    def __str__(self):
        return f"[{self.id}] {self.title}"
