# Generated by Django 4.0.3 on 2022-03-24 10:45

import uuid

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="ExerciseText",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("title", models.TextField()),
                ("text", models.TextField()),
                ("source", models.URLField(null=True)),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
