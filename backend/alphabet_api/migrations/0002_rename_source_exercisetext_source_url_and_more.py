# Generated by Django 4.0.3 on 2022-03-24 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alphabet_api', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exercisetext',
            old_name='source',
            new_name='source_url',
        ),
        migrations.AddField(
            model_name='exercisetext',
            name='source_description',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
