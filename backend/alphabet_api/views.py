import uuid

from django.http import HttpRequest, JsonResponse
from django.shortcuts import get_object_or_404

from .models import ExerciseText


def index(request: HttpRequest) -> JsonResponse:
    return JsonResponse(
        {
            "texts": [
                {
                    "id": entry.id,
                    "title": entry.title,
                    "preview_url": entry.preview.url if entry.preview else None,
                }
                for entry in ExerciseText.objects.all()
            ]
        }
    )


def exercise(request: HttpRequest, exercise_id: uuid.UUID) -> JsonResponse:
    exercise_obj = get_object_or_404(ExerciseText, id=exercise_id)
    next_exercise = ExerciseText.objects.order_by("?").first().id

    return JsonResponse(
        {
            "title": exercise_obj.title,
            "text": exercise_obj.text,
            "source_url": exercise_obj.source_url,
            "source_description": exercise_obj.source_description,
            "next_exercise_id": next_exercise,
        }
    )
