from django.contrib import admin

from . import models

admin.site.register(models.User)
admin.site.register(models.UserSession)
admin.site.register(models.ValidationEmailMessage)
