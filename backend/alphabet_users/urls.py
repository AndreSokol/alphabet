from django.urls import path

from . import views

app_name = "users"

urlpatterns = [
    path("request-code/", views.RequestCodeView.as_view()),
    path("submit-code/", views.SubmitCodeView.as_view()),
    path("me/", views.MeView.as_view()),
]
