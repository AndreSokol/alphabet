import functools
import json
import random

from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.http import (HttpRequest, HttpResponse, HttpResponseBadRequest,
                         JsonResponse)
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from core.views import CorsView

from .models import User, UserSession, ValidationEmailMessage


@method_decorator(csrf_exempt, name="dispatch")
class RequestCodeView(CorsView):
    def post(self, request: HttpRequest) -> JsonResponse:
        data = json.loads(request.body)
        new_message = ValidationEmailMessage(
            email=data["email"],
            code="000000",
            # code=str(random.randint(100_000, 1_000_000))
        )

        try:
            new_message.full_clean()
            new_message.save()
        except ValidationError:
            return JsonResponse({"error": "bad email"}, status=400)

        send_mail(
            "Тема письма",
            f"Текст письма. {new_message.code}",
            "from@example.com",  # Это поле "От кого"
            [new_message.email],  # Это поле "Кому" (можно указать список адресов)
            fail_silently=False,  # Сообщать об ошибках («молчать ли об ошибках?»)
        )

        return JsonResponse({})


@method_decorator(csrf_exempt, name="dispatch")
class SubmitCodeView(CorsView):
    def post(self, request: HttpRequest) -> JsonResponse:
        data = json.loads(request.body)

        message = (
            ValidationEmailMessage.objects.filter(
                email=data["email"], code=data["code"]
            )
            .order_by("-sent_at")
            .first()
        )

        if not message:
            return JsonResponse({"error": "bad code"}, status=400)

        user, is_created = User.objects.get_or_create(email=message.email)
        if is_created:
            print("NEW USER:", user)

        session = UserSession()
        session.user = user
        session.save(force_insert=True)

        return JsonResponse({"session": session.id})


def with_session(func):
    @functools.wraps(func)
    def wrapper(self, request: HttpRequest, *args, **kwargs):
        session = request.headers.get("X-Alphabet-Session")
        if not session:
            return JsonResponse({"error": "login required"}, status=401)

        try:
            session = UserSession.objects.select_related("user").get(id=session)
        except (UserSession.DoesNotExist, ValidationError):
            return JsonResponse({"error": "bad session"}, status=401)

        request.alpha_session = session
        return func(self, request, *args, **kwargs)

    return wrapper


class MeView(CorsView):
    @with_session
    def get(self, request: HttpRequest) -> JsonResponse:
        return JsonResponse({"user": str(request.alpha_session.user)})
