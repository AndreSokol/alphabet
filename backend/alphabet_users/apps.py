from django.apps import AppConfig


class AlphabetUsersConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "alphabet_users"
