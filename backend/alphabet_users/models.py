import uuid

from django.db import models

from core.models import UUIDKeyedModel


class User(UUIDKeyedModel):
    email = models.EmailField()
    registered_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email


class UserSession(UUIDKeyedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    started = models.DateTimeField(auto_now_add=True)


class ValidationEmailMessage(UUIDKeyedModel):
    email = models.EmailField()
    sent_at = models.DateTimeField(auto_now_add=True)
    code = models.SlugField()
