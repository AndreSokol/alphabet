from django.http import HttpRequest, HttpResponse
from django.views import View


class CorsView(View):
    def options(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        response = HttpResponse()
        response.headers["Allow"] = ", ".join(self._allowed_methods())
        response.headers["Content-Length"] = "0"
        response.headers["Access-Control-Allow-Headers"] = "*"
        response.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response.headers["Access-Control-Allow-Origin"] = "*"
        response.headers["Access-Control-Max-Age"] = f"{24 * 60 * 60}"  # 24 hours

        return response
