from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("api/", include("alphabet_api.urls", namespace="api")),
    path("api/auth/", include("alphabet_users.urls", namespace="users")),
    path("admin/", admin.site.urls),
]
