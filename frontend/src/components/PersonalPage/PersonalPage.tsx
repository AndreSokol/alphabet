import React, { useEffect, useState } from "react";
import { Button, Input } from "antd";
import axios from "axios";

enum LoginStage {
  ENTER_EMAIL = 0,
  ENTER_CODE,
  SUCCESS,
}

const requestLoginCode = (email: string) =>
  axios
    .post("http://192.168.100.3:8000/api/auth/request-code/", {
      email: email,
    })
    .then((res) => res.data)
    .catch((err) => console.log(err.response.status, err.response.data));

const submitLoginCode = (email: string, code: string) =>
  axios
    .post("http://192.168.100.3:8000/api/auth/submit-code/", {
      email: email,
      code: code,
    })
    .then((res) => res.data)
    .catch((err) => console.log(err.response.status, err.response.data));

const PersonalPage = () => {
  const [username, setUsername] = useState("");
  const [code, setCode] = useState("");
  const [step, setStep] = useState(LoginStage.ENTER_EMAIL);

  return (
    <div style={{ maxWidth: 800, margin: "0 auto" }}>
      <h1>Login: {username}</h1>
      <Input
        size="large"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        disabled={step !== LoginStage.ENTER_EMAIL}
      />
      <Button
        onClick={() => {
          requestLoginCode(username).then(() => setStep(LoginStage.ENTER_CODE));
        }}
        disabled={step !== LoginStage.ENTER_EMAIL}
      >
        Получить код
      </Button>

      <Input
        size="large"
        value={code}
        onChange={(e) => setCode(e.target.value)}
        disabled={step !== LoginStage.ENTER_CODE}
      />

      <Button
        onClick={() => {
          submitLoginCode(username, code).then((data) => {
            setStep(LoginStage.SUCCESS);
            console.log(data);
          });
        }}
        disabled={step !== LoginStage.ENTER_CODE}
      >
        Войти
      </Button>
    </div>
  );
};

export default PersonalPage;
