import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import { FetchExerciseLinks, IExerciseLink } from "../../app/api";
import styles from "./MainPage.module.css";

const MainPage = () => {
  const [links, setLinks] = useState<IExerciseLink[]>([]);

  useEffect(() => {
    FetchExerciseLinks().then((links) => setLinks(links));
  }, []);

  return (
    <>
      <div className={styles.global_container}>
        <div className={styles.read_georgian}>
          <h1>READ GEORGIAN</h1>
          <p>
            Мы сделали этот сайт, чтобы помочь тем, кто хочет погрузиться в
            культуру и традиции Грузии, познакомиться с языком, сделать первый и
            самый важный шаг в изучении грузинского – выучить алфавит.
          </p>
          <p>
            Звуки в грузинском языке часто похожи на те, что есть в русском
            (хотя есть и те, что ощутимо отличаются и вводят в ступор). В этом
            упражнении вы можете заменить в тексте часть букв русского алфавита
            на буквы грузинского. Так буквы и соответствующие им звуки будут
            запоминаться быстрее.
          </p>
          <p>
            Тексты упражнений на этом сайте – рассказы об истории и традициях
            Грузии. Так что, изучая алфавит, вы еще познакомитесь с культурой
            этой удивительной страны.
          </p>
        </div>

        <div className={styles.exercises_container}>
          <h1>Выберите один из текстов</h1>
          <div className={styles.exercise_block_container}>
            {links.map((linkData) => (
              <Link
                key={linkData.id}
                to={`/text/${linkData.id}`}
                className={styles.exercise_block}
                style={{
                  backgroundImage: `linear-gradient(rgba(0,0,0,0) 50%, rgba(0,0,0,.7) 100%), 
                                    url(${linkData.preview_url})`,
                }}
              >
                <p>{linkData.title}</p>
              </Link>
            ))}
          </div>
        </div>
      </div>
      <div className={styles.footer}>
        <div className={styles.global_container}>
          <p>
            Разработка сайта и дизайн на коленках
            <br />
            by Андрей Соколов и Катя Савицкая
          </p>
          <p>
            Тбилиси, Грузия
            <br />
            2022
          </p>
        </div>
      </div>
    </>
  );
};

export default MainPage;
