import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import { GetFormattedHtml, GetFormattedLine } from "../../data";
import styles from "./FormattedContainer.module.css";
import { useAppSelector } from "../../app/hooks";
import { selectEnabled } from "../../app/formattingSettingsSlice";
import { FetchSingleExercise, IExerciseData } from "../../app/api";

const Arrow = () => (
  <svg
    width="100"
    height="24"
    viewBox="0 0 100 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2 10.5C1.17157 10.5 0.5 11.1716 0.5 12C0.5 12.8284 1.17157 13.5 2 13.5V10.5ZM99.0607 13.0607C99.6464 12.4749 99.6464 11.5251 99.0607 10.9393L89.5147 1.3934C88.9289 0.807612 87.9792 0.807612 87.3934 1.3934C86.8076 1.97918 86.8076 2.92893 87.3934 3.51472L95.8787 12L87.3934 20.4853C86.8076 21.0711 86.8076 22.0208 87.3934 22.6066C87.9792 23.1924 88.9289 23.1924 89.5147 22.6066L99.0607 13.0607ZM2 13.5H98V10.5H2V13.5Z"
      fill="white"
    />
  </svg>
);

const FormattedContainer = () => {
  const enabledRules = useAppSelector(selectEnabled);
  const [exerciseData, setExerciseData] = useState<IExerciseData | undefined>();
  const { textId } = useParams<"textId">();

  useEffect(() => {
    FetchSingleExercise(textId as string).then((res) => {
      setExerciseData(res);
      window.scroll({ left: 0, top: 0 });
    });
  }, [textId]);

  if (!exerciseData) {
    return <>Loading...</>;
  }

  return (
    <div className={styles.formatted_container}>
      <h1>{GetFormattedLine(exerciseData.title, enabledRules)}</h1>
      <div
        dangerouslySetInnerHTML={{
          __html: GetFormattedHtml(exerciseData.text, enabledRules),
        }}
      />
      <p className={styles.source}>
        {exerciseData.source_description}
        <br />
        {exerciseData.source_url}
      </p>

      <p className={styles.disclaimer_container}>
        Дисклеймер: текст дан для запоминания букв грузинского алфавита.
        Реальное написание имен собственных может отличаться. Когда-нибудь мы
        обязательно придумаем, как это поправить
      </p>

      <Link
        className={styles.next_text_button}
        to={`/text/${exerciseData.next_exercise_id}/`}
      >
        <Arrow />
      </Link>
    </div>
  );
};

export default FormattedContainer;
