import React, { useState } from "react";

import { ConversionRules, ConversionRuleT } from "../../data";
import styles from "./ComplexityBar.module.css";

import {
  selectEnabled,
  toggleEnabled,
} from "../../app/formattingSettingsSlice";
import { useAppDispatch, useAppSelector } from "../../app/hooks";

const ComplexityBarBlock = (props: { rule: ConversionRuleT }) => {
  const { rule } = props;

  const enabledConversionRules = useAppSelector(selectEnabled);
  const [hovered, setHovered] = useState(false);
  const dispatch = useAppDispatch();

  const selected = enabledConversionRules.includes(rule.idx);
  const className = selected
    ? styles.button + " " + styles.button__selected
    : styles.button;

  const showGeorgian = (hovered && !selected) || selected;

  return (
    <div
      className={className}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onClick={() => dispatch(toggleEnabled(rule.idx))}
    >
      {showGeorgian ? rule.to : rule.from}
    </div>
  );
};

const ComplexityBar = () => {
  return (
    <div>
      <p className={styles.choose_title}>
        Выберите буквы, чтение которых вы хотите потренировать
      </p>
      <div className={styles.options_container}>
        {ConversionRules.map((rule) => (
          <ComplexityBarBlock key={rule.idx} rule={rule} />
        ))}
      </div>
    </div>
  );
};

export default ComplexityBar;
