import React from "react";
import { Routes, Route } from "react-router-dom";

import "./App.css";
import FormattedContainer from "./components/FormattedContainer/FormattedContainer";
import ComplexityBar from "./components/ComplexityBar/ComplexityBar";
// import PersonalPage from "./components/PersonalPage/PersonalPage";
import MainPage from "./components/MainPage/MainPage";

const MainApp = () => (
  <div className="pure-g container">
    <div className="pure-u-1">
      <ComplexityBar />
      <FormattedContainer />
    </div>
  </div>
);

function App() {
  return (
    <Routes>
      <Route path={"/"} element={<MainPage />} />
      <Route path={"text/:textId/"} element={<MainApp />} />
      <Route path={"*"} element={<h1>404</h1>} />
    </Routes>
  );
}

export default App;
