import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "./store";

export interface IFormattingSettings {
  enabled: string[];
}

const initialState: IFormattingSettings = {
  enabled: [],
};

export const formattingSettingsSlice = createSlice({
  name: "formattingSettings",
  initialState,
  reducers: {
    toggleEnabled: (state, action: PayloadAction<string>) => {
      // FIXME: cleanup

      const optionId = action.payload;
      console.log("Toggle", optionId);
      const enabledSet = new Set(state.enabled);
      if (enabledSet.has(action.payload)) enabledSet.delete(action.payload);
      else enabledSet.add(action.payload);

      state.enabled = Array.from(enabledSet);
    },
  },
});

export const { toggleEnabled } = formattingSettingsSlice.actions;

export const selectEnabled = (state: RootState) =>
  state.formattingSettings.enabled;

export default formattingSettingsSlice.reducer;
