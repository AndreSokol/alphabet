import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import formattingSettingsReducer from "./formattingSettingsSlice";

export const store = configureStore({
  reducer: {
    formattingSettings: formattingSettingsReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
