import axios from "axios";

const API_URL = process.env.REACT_APP_API_URL || "";


export interface IExerciseData {
  title: string;
  text: string;
  source_url: string | undefined;
  source_description: string | undefined;
  next_exercise_id: string | undefined;
}

export const FetchSingleExercise = (exerciseId: string) =>
  axios
    .get(`${API_URL}/api/exercise/${exerciseId}/`)
    .then((res) => {
      return {
        title: res.data.title,
        text: res.data.text,
        source_url: res.data.source_url,
        source_description: res.data.source_description,
        next_exercise_id: res.data.next_exercise_id
      } as IExerciseData;
    })
    .catch((err) => {
      console.error("Error:", err);
      throw err;
    });

export interface IExerciseLink {
  id: string;
  title: string;
  preview_url: string | undefined;
}

export const FetchExerciseLinks = () =>
  axios
    .get(`${API_URL}/api/`)
    .then((res) => {
      return res.data.texts.map(
        (entry: {
          preview_url: string | undefined;
          id: any; title: any
        }) =>
          ({
            id: entry.id,
            title: entry.title,
            preview_url: entry.preview_url
          } as IExerciseLink)
      );
    })
    .catch((err) => {
      console.error("Error:", err);
      throw err;
    });
